.. _optimize:

=======================
Optimization of Density
=======================

We can easily perform a density optimization using `dftpy` script with a `config.ini` file.

.. highlight:: bash

::

    $ dftpy -i config.ini

