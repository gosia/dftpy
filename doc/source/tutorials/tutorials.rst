.. _tutorials:

Tutorials
=========

Config of DFTpy script
----------------------

.. toctree::
   :maxdepth: 1

   config

OFDFT
-----

.. toctree::
   :maxdepth: 1

   ofdft/optimize
   ofdft/relax
   ofdft/md

TDDFT
-----

.. toctree::
   :maxdepth: 1

   tddft/propagate

Do it on a Jupyter Notebook!
----------------------------


.. toctree::
   :maxdepth: 1

   jupyter/optimization

